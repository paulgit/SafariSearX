# searX Safari Extension

This safari extension will add a toolbar icon that will allow you to search your favourite searX instance. Optionally you can add you favourite searX instance as the search provider in the Safari address bar.

## What's this then?

As of Safari 6, the separate address bar and search field have been combined into a single "Smart Search Field" that can take both web addresses and searches. In the Safari Preferences, you can set the url of your favourite searX instance.  You can find public searX instances [here](https://github.com/asciimoo/searx/wiki/Searx-instances).

## What is searX?

searX is a free internet metasearch engine which aggregates results from more than 70 search services. Users are neither tracked nor profiled. Additionally, searX can be used over Tor for online anonymity.

More information can be found [here](https://asciimoo.github.io/searx/)

## Download

SafariSearX can be downloaded from [here](https://gitlab.com/paulgit/SafariSearX/tags)